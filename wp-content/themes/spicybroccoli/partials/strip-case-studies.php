<div class="bg-grey">
	<div class="wrapper cf">
		<h2>Case Studies</h2>
		<div class="what-we-do__grid">
			<?php $projects_query = array(
				'post_type' => 'case_study',
				'posts_per_page' => -1
				);

			$projects = new WP_Query($projects_query);
			?>
			<?php if ($projects->have_posts()): ?>
				<div class="packery">
				<?php while($projects->have_posts()): $projects->the_post(); ?>
					<?php $class = 'grid-item'; ?>
			    	<a href="<?php the_permalink(); ?>" class="<?php echo $class; ?>">
						<?php if (class_exists('MultiPostThumbnails')) :
						    MultiPostThumbnails::the_post_thumbnail(
						        get_post_type(),
						        'grid-image'
						    );
						endif; ?>
						<div class="project-title"><span><?php the_title(); ?></span></div>
			    	</a>
			    <?php endwhile; ?>
			    </div>
			<?php endif; ?>
		</div>
	</div><!-- .wrapper -->
</div><!-- .bg-grey -->