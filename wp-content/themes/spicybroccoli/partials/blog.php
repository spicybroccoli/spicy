<div class="what-we-think">
	<div class="row">
		<div class="col span6 blog-block-1">
			<div class="row">
				<?php $latest = new WP_Query('posts_per_page=1'); ?>
				<?php if($latest->have_posts()): while($latest->have_posts()): $latest->the_post(); ?>
				<div class="block-blog">
					<div class="block-blog-author"><?php the_author(); ?></div>
					<div class="block-blog-date"><?php echo get_the_date("M j, Y"); ?></div>
					<div class="block-blog-title"><?php the_title(); ?></div>
					<div class="block-blog-featuredimage"><?php the_post_thumbnail(); ?></div>
					<div class="block-blog-excerpt"><?php echo wp_trim_words_retain_formatting(get_the_content(), 60); ?></div>
					<div class="block-blog--readmore"><a href="<?php the_permalink(); ?>" class="read-more">Read More</a></div>
				</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
		<div class="col span1">&nbsp;</div>
		<div class="col span5 blog-block-4">
			<div class="row col span12">
				<?php $latest = new WP_Query('posts_per_page=4&offset=1'); ?>
				<?php if($latest->have_posts()): while($latest->have_posts()): $latest->the_post(); ?>
				<a href="<?php the_permalink(); ?>" class="block-blog col span6">
					<div class="block-blog-author"><?php the_author(); ?></div>
					<div class="block-blog-date"><?php echo get_the_date("M j, Y"); ?></div>
					<div class="block-blog-title"><?php echo wp_trim_words(get_the_title(), 5); ?></div>
				</a>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</div>