<?php

$grid = new WP_Query(
	array(
		'post_type' => grid_post_types(), 
		'meta_key' => '_show_on_homepage',
		'meta_value' => '1',
		'posts_per_page' => 9
	)
);
?>

<?php if ( $grid->have_posts() ): ?>
	<div class="grid">
	<?php while( $grid->have_posts() ): $grid->the_post(); ?>
		<a href="<?php the_permalink(); ?>" class="grid-tile">
<?php echo get_the_post_thumbnail(); ?> 
       
			<div class="project-title"><span><?php the_title(); ?></span></div>
		</a>
	<?php endwhile; ?>
	</div>
	<?php endif;

?>