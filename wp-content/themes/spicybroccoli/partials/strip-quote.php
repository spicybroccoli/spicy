<div class="free-stuff" style="background:none;padding: 1rem 0;">
	<div class="wrapper">

		<form class="wf-form" action="<?php echo site_url('/contact/submit'); ?>" method="post" data-parsley-validate>

		<h2>Need a Quote?</h2>
		<h5>Fill in the form below and we'll get on it.</h5>

		<div class="loader-container">
			<div class="loader"></div>
		</div>

		<div class="field-row">
			<span class="field">
				<label>Your First Name*</label>
				<input type="text" data-trigger="change" name="FIRST_NAME" required>
			</span>

			<span class="field">
				<label>Your Last Name*</label>
				<input type="text" name="LAST_NAME" required>
			</span>
		</div>

		<div class="field-row">
			<span class="field">
				<label>Your Email*</label>
				<input type="email" data-parsley-trigger="change" data-trigger="change" data-type="email" name="EMAIL" required>
			</span>

			<span class="field">
				<label>Your Phone*</label>
				<input type="text" name="PHONE" required>
			</span>
		</div>

		<div class="field-row">
			<label>Your Enquiry*</label>
			<select name="ENQUIRY_TYPE" required>
				<option value="" disabled="" selected="">Please select one</option>
				<option value="I need it done yesterday!">I need it done yesterday!</option>
				<option value="I have a massive budget!">I have a massive budget!</option>
				<option value="I want to be your Intern/Coffee Slave">I want to be your Intern/Coffee Slave</option>
				<option value="Website feedback">Website feedback</option>
			</select>
		</div>

		<div class="field-row">
			<label>How did you find us?</label>
			<select name="REFERRAL">
				<option value="" disabled="" selected="">Please select one</option>
				<option value="Google">Google</option>
				<option value="Referral / Friend">Referral / Friend</option>
				<option value="BNI Referral">BNI Referral</option>
				<option value="Linked from a website we built">Linked from a website we built</option>
				<option value="Thought our name was cool">Thought our name was cool</option>
			</select>
		</div>

		<div class="field-row">
			<label>Your Message*</label>
			<textarea name="NOTE" cols="87" rows="8" required></textarea>
		</div>

		<p>
			<input type="text" style="display:none" name="submission">
			<br>
  			<input type="submit" value="Send" class="submit">
  		</p>
</form>

	<div class="wf-response">
		
	</div>

	</div><!-- .wrapper -->
</div><!-- .free-stuff -->