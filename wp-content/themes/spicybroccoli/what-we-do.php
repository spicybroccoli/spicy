<?php

/**

 * 

 * @package Spicy Broccoli

 * @since Spicy Broccoli 1.0

 * @version 1.0

 */



get_header(); ?>

<div class="bg-white">
  <div class="wrapper cf">
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <div class="what-we-do__menu">
        <?php wp_nav_menu( array( 'theme_location' => 'project-menu' ) ); ?>
      </div>
      <div class="what-we-do__grid">
        <div class="packery">
          <?php while ( have_posts() ) : the_post(); ?>
          <?php $class = ( get_post_meta(get_the_ID(), '_featured', true) == 1 ) ? 'grid-item w2 h2' : 'grid-item'; ?>
          <a href="<?php the_permalink(); ?>" class="<?php echo $class; ?>">
          <?php if (class_exists('MultiPostThumbnails')) :

											    MultiPostThumbnails::the_post_thumbnail(

											        get_post_type(),

											        'grid-image'

											    );

											endif; ?>
          <div class="project-title"><span>
            <?php the_title(); ?>
            </span></div>
          </a>
          <?php endwhile; ?>
        </div>
      </div>
      <div class="entry-utility">
        <?php edit_post_link('Edit', '<span class="edit-link">', '</span>' ); ?>
      </div>
      <!-- .entry-utility --> 
      
    </div>
    <!-- #post-## --> 
    
  </div>
  <!-- .wrapper --> 
  
</div>
<!-- .bg-white-->

<?php get_template_part('partials/strip-client'); ?>
<?php get_template_part('partials/strip-contact'); ?>
<?php get_footer(); ?>
