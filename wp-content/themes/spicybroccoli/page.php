<?php
/**
 * The default Page template
 * 
 * @package Spicy Broccoli
 * @since Spicy Broccoli 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="bg-page">
		<div class="wrapper cf">
			<?php while ( have_posts() ) : the_post(); ?>
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="col-left">
                            <h1><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( 'Permalink to %s', the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>

							<div class="entry-content">
								<?php the_content( 'Continue reading <span class="meta-nav">&rarr;</span>'); ?>
								<?php wp_link_pages( array( 'before' => '<div class="page-link">' .  'Pages:' , 'after' => '</div>' ) ); ?>
							</div><!-- .entry-content -->

							</div>
                            <div class="col-right">
                            <div class="related-info">
                            <h3>Related Info</h3>
                            	<nav>
                                    <ul>
                                    	<?php
											$post_parent = empty( $post->post_parent ) ? $post->ID : $post->post_parent;
											$args = array(
												'post_type'      => 'page',
												'posts_per_page' => -1,
												'post_parent'    => $post_parent,
												'order'          => 'ASC',
												'orderby'        => 'menu_order'
											 );
											
											
											$parent = new WP_Query( $args );
											
											if ( $parent->have_posts() ) : ?>
											
												<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                                                	
                                                     <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
											
												<?php endwhile; ?>
											
											<?php endif; wp_reset_query(); ?>
                                    </ul>
                                </nav>
                                </div>
                                <h3>Related Projects</h3>
                                 <div class="what-we-do__grid">
       						 <?php $projects_query = array(
									'post_type' => 'project',
									'posts_per_page' => 4,
									'orderby' => 'rand'
									);
								if ($_GET['project-type']) {
									$projects_query['project_type'] = $_GET['project-type'];
								}
								$projects = new WP_Query($projects_query);
								?>
								<?php if ($projects->have_posts()): ?>
                                <div class="packery">
                                  <?php while($projects->have_posts()): $projects->the_post(); ?>
                                  <?php $class = ( get_post_meta(get_the_ID(), '_featured', true) == 1 ) ? 'grid-item w2 h2' : 'grid-item'; ?>
                                  <a href="<?php the_permalink(); ?>" class="<?php echo $class; ?>">
                                  <?php if (class_exists('MultiPostThumbnails')) :
                        
                                                                        MultiPostThumbnails::the_post_thumbnail(
                        
                                                                            get_post_type(),
                        
                                                                            'grid-image'
                        
                                                                        );
                        
                                                                    endif; ?>
                                  <div class="project-title"><span>
                                    <?php the_title(); ?>
                                    </span></div>
                                  </a>
                                  <?php endwhile; ?>
                                </div>
                                <?php endif; ?>
                              </div>
                            </div>
						</div><!-- #post-## -->


				<?php endwhile; // End the loop. Whew. ?>

		</div><!-- .wrapper -->
	</div><!-- .bg-white-->

	<?php get_template_part('partials/strip-case-studies'); ?>

	<?php get_template_part('partials/strip-contact'); ?>

<?php get_footer(); ?>