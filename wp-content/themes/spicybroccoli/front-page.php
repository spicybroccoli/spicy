<?php
/**
 * The Front Page template
 * 
 * @package Spicy Broccoli
 * @since Spicy Broccoli 1.0
 * @version 1.0
 */

get_header(); ?>

	<div class="bg-white">
		<div class="wrapper cf">
		<?php get_template_part('partials/grid'); ?>

<!-- -->
<!-- -->

		
		</div>
	</div>
	<div class="bg-abstract what-why-we-do">
		<div class="wrapper">
			<h2>why 300+ clients have trusted us with their brand</h2>
            <p class="quotes">We create memorable brand journeys which immediately increase the value of our clients’ businesses.</p>
			<a href="#">for real? yep. find out how.  ></a>
		</div><!-- .wrapper -->
	</div><!-- .bg-grey -->
	<div class="bg-white awesome-at">
		<div class="wrapper cf">
			<h2>what we’re awesome at</h2>
			<p>Creating memorable brand journeys which increase the value of our clients’ businesses</p>
			<div class="box three-col">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons_what_logo.png">
				<h3>logos</h3>
				<h5 class="bright">We put the fun in <br />
			    fundamentals</h5>
				<a class="button" href="http://dev.spicybroccolitesting.com.au/spicy/what-we-do/logos">See Logos</a>
			</div>
			<div class="box three-col">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons_what_website.png">
				<h3>websites</h3>
				<h5 class="bright">Code, sweat</br>and pixels</h5>
				<a class="button" href="http://dev.spicybroccolitesting.com.au/spicy/what-we-do/websites">See Websites</a>
			</div>
			<div class="box three-col">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons_what_branding.png">
				<h3>branding</h3>
				<h5 class="bright">You can't move up <br />
			    if you don't stand out!</h5>
				<a class="button" href="http://dev.spicybroccolitesting.com.au/spicy/what-we-do/branding">See Branding</a>
			</div>

		</div><!-- .wrapper -->
	</div><!-- .bg-white -->
	<div class="bg-grey bg-case-studies">
		<div class="wrapper">
			<h2>case studies</h2>
			<?php echo do_shortcode('[metaslider id=80]'); ?>
		</div><!-- .wrapper -->
	</div><!-- .bg-grey -->
    <?php /* ## COMMENT FREE STAFF SECTION
	<div class="free-stuff">
		<div class="wrapper">
			<h2>Free Stuff</h2>
			<h5>Discover the seven <span>hot tips</span> to making your business stand out from the competition.</h5>
			<p>Get one hot tip delivered weekly to your inbox.</br>Just fill in the form below. <strong>We don't spam.</strong></p>
			<?php get_template_part('partials/mc-subscribe'); ?>
		</div><!-- .wrapper -->
	</div><!-- .free-stuff -->
	*/ ?>

<div class="bg-white all-in-one">
  <div class="wrapper">
   	<h2>your all-in-one creative team</h2>
    <ul class="all-in-one-list">
            	<li><h4>Instant Quotes</h4>
      <p>Send an enquiry and get get a reply within 24 hours.<br>
 Jobs quoted per hour/per project.</p></li>
                <li><h4>Websites guaranteed </h4>
                <p>Get the message right.<br>
Get the functionality right.<br>
Free fixes for 6 weeks.</p></li>
                <li><h4>Local team</h4>
                <p>Responsive.<br>
Real-time communication with real people. </p></li>
                <li><h4>Quick turnaround</h4>
                <p>No fuss.<br>Deal directly with our expert designers and account managers. </p></li>
                <li><h4>Immediate results</h4>
                <p>Watch profits rise. <br>Increase web traffic.<br>Insight through analysis.</p></li>
                <li><h4>Free, secure management</h4>
                <p>Open source.<br>
Easy to use content management.<br>
Scalable - start small and build up.</p></li>
            </ul>
    </div>
    <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/img_book.jpg" width="640" height="175"  alt=""/></a>
    </div>
    <div class="bg-grey bg-abstract about-us">
		<div class="wrapper cf">
			<h2>more about us</h2>
			<div class="fl four-col">
				<h3>We are a Sydney graphic design & web design studio.</h3>
				<h6 class="bright">We are a <span>fun team</span> who develop creative designs for our clients.</h6>
			</div>
			<div class="fr seven-col">
				<?php $latest_testimonial = new WP_Query(array(
					'post_type' => 'testimonial',
					'posts_per_page' => 1,
				)); ?>
				<?php if( $latest_testimonial->have_posts() ): while( $latest_testimonial->have_posts() ): $latest_testimonial->the_post(); ?>
				<div class="item item-wbtn three-col">
					<div class="item-wtestimonial">
						<div class="block-blog-author"><?php echo wp_trim_words( get_the_title(), 1, ''); ?></div>
						<div class="block-blog-date block-blog-date--regularcase"><?php echo wp_trim_words(get_post_meta(get_the_ID(), 'testimonial_client_after', true), 3, ''); ?></div>
						<div class="block-blog-title"><p class="quotes quote-top"><?php echo wp_trim_words(get_post_meta(get_the_ID(), 'testimonial_client_text', true), 12, ''); ?></p></div>
					</div>
					<a class="button" href="<?php echo the_permalink(); ?>">what people say</a>
				</div>
				<?php endwhile; endif; ?>
				<div class="item item-wbtn three-col">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/btn_staff.jpg">
					<a class="button" href="<?php echo site_url('/who-we-are/our-team'); ?>">see our staff</a>
				</div>
				<div class="item item-wbtn three-col">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/btn_history.gif">
					<a class="button" href="<?php echo site_url('/who-we-are/our-vision'); ?>">read our history</a>
				</div>
			</div>
		</div><!-- .wrapper -->
	</div><!-- .bg-grey .about-us -->
	<div class="bg-white">
		<div class="wrapper cf">
			<h2>what we think</h2>
			<p>There is a lot going on at Spicy and we are very happy to share our thoughts and ideas with you!</p>
			<?php get_template_part('partials/blog'); ?>
		</div><!-- .wrapper -->
	</div><!-- .bg-white -->

<?php get_footer(); ?>