<?php
/**
 * The main template file.
 *
 * @package Spicy Broccoli
 * @since Spicy Broccoli 1.0
 * @version 1.0
 */

get_header(); ?>
	<div class="bg-white">
		<div class="wrapper cf">
			<!--<div id="content" role="main">-->
			<div class="seven-col fl">

			<?php while ( have_posts() ) : the_post(); ?>
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( 'Permalink to %s', the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

						<div class="entry-meta">
							<span class="entry-meta-author"><?php the_author(); ?></span><span class="entry-meta-date"><?php echo get_the_date("M j, Y"); ?></span>
							<?php
								$tags_list = get_the_tag_list( '', ', ' );
								if ( $tags_list ):
							?>
								<span class="tag-links">
									<?php echo  $tags_list; ?>
								</span>
							<?php endif; ?>
						</div><!-- .entry-meta -->

				<?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>
						<div class="entry-summary">
							<?php the_post_thumbnail(); ?>
							<?php the_excerpt(); ?>
							<div class="block-blog--readmore"><a href="<?php the_permalink(); ?>" class="read-more">Read More</a></div>
						</div><!-- .entry-summary -->
				<?php else : ?>
						<div class="entry-content">
							<?php the_content( 'Continue reading <span class="meta-nav">&rarr;</span>'); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-link">' .  'Pages:' , 'after' => '</div>' ) ); ?>
						</div><!-- .entry-content -->
				<?php endif; ?>

					<?php if ( !is_archive() ) : // Do not display entry-utility for archives. ?>
						<div class="entry-utility">
							<span class="entry-meta-author"><?php the_author(); ?></span><span class="entry-meta-date"><?php the_date(); ?></span>
							<?php
								$tags_list = get_the_tag_list( '', ', ' );
								if ( $tags_list ):
							?>
								<span class="tag-links">
									<?php printf( '<span class="%1$s">Tagged</span> %2$s', 'entry-utility-prep entry-utility-prep-tag-links', $tags_list ); ?>
								</span>
								<span class="meta-sep">|</span>
							<?php endif; ?>
       
							<span class="comments-link"><?php comments_popup_link( 'Leave a comment', '1 Comment', '% Comments' ); ?></span>
							<?php edit_post_link('Edit', '<span class="meta-sep">|</span> <span class="edit-link">', '</span>' ); ?>
						</div><!-- .entry-utility -->
						
						<div class="entry-share">
							<!-- Go to www.addthis.com/dashboard to customize your tools -->
							<div class="addthis_sharing_toolbox"><span>Share it</span></div>
						</div><!-- .entry-share -->

					<?php endif; ?>

					</div><!-- #post-## -->

			<?php endwhile; // End the loop. Whew. ?>
			
			<div id="pagination">
				<?php paginate(); ?>
			</div>

			</div><!-- #content -->
			<?php get_sidebar(); ?>
		</div><!-- .wrapper -->
	</div><!-- .bg-white-->

<?php get_footer(); ?>