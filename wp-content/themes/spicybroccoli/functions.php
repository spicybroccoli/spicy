<?php

/**

 * Theme Functions

 *

 * @package Spicy Broccoli

 * @since Spicy Broccoli 1.0

 * @version 1.0

 */

remove_filter('template_redirect', 'redirect_canonical');



// Bootstrap Theme

include('inc/theme-setup.php'); // Theme Setup



include('inc/template-utils.php'); // Load Template Utilities

include('inc/breadcrumbs.php'); // Load Breadcrumbs 

include('inc/branding.php'); // Load Template Utilities

include('inc/responsive-nav.php'); // Responsive Nav

// include('inc/landing.php'); // Load Landing page if necessary

include('inc/scripts.php'); // Load Scripts



// Bootstrap Spicy

include('inc/metabox-generic.php'); // Generic metaboxes



include('inc/cpt/cpt-projects.php'); // Load Projects CPT

include('inc/cpt/cpt-case_study.php'); // Load Case Studies CPT

include('inc/cpt/cpt-testimonials.php'); // Load Testimonials CPT



include('inc/cpt/metabox-projects.php'); // Load Projects Metaboxes

include('inc/cpt/metabox-case_study.php'); // Load Projects Metaboxes



include('inc/tax/tax-project_type.php'); // Load Project Type Taxonomy

include('inc/tax/tax-project_client.php'); // Load Project Client Taxonomy



include('inc/routes.php'); // Load our custom routes



add_action('init', function(){

  if (is_admin()) {

    wp_dequeue_script('case-study-website-styles');

  }

});



add_filter('body_class', function( $classes ) {

  $classes[] = 'no-js';

  return $classes;

});



function grid_post_types() {

  return array('post', 'page', 'project', 'case_study');

}



// Enable Featured Gallery on Project CPT

add_filter('fg_post_types', function(){ return grid_post_types(); });



function wp_trim_words_retain_formatting( $text, $num_words = 55, $more = null ) {

                    if ( null === $more )

                        $more = __( '&hellip;' );

                    $original_text = $text;

                    /* translators: If your word count is based on single characters (East Asian characters),

                       enter 'characters'. Otherwise, enter 'words'. Do not translate into your own language. */

                    if ( 'characters' == _x( 'words', 'word count: words or characters?' ) && preg_match( '/^utf\-?8$/i', get_option( 'blog_charset' ) ) ) {

                        $text = trim( preg_replace( "/[\n\r\t ]+/", ' ', $text ), ' ' );

                        preg_match_all( '/./u', $text, $words_array );

                        $words_array = array_slice( $words_array[0], 0, $num_words + 1 );

                        $sep = '';

                    } else {

                        $words_array = preg_split( "/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY );

                        $sep = ' ';

                    }

                    if ( count( $words_array ) > $num_words ) {

                        array_pop( $words_array );

                        $text = implode( $sep, $words_array );

                        $text = $text . $more;

                    } else {

                        $text = implode( $sep, $words_array );

                    }

                    /**

                     * Filter the text content after words have been trimmed.

                     *

                     * @since 3.3.0

                     *

                     * @param string $text          The trimmed text.

                     * @param int    $num_words     The number of words to trim the text to. Default 5.

                     * @param string $more          An optional string to append to the end of the trimmed text, e.g. &hellip;.

                     * @param string $original_text The text before it was trimmed.

                     */

                    return apply_filters( 'wp_trim_words', $text, $num_words, $more, $original_text );

                }





function paginate() {

    global $wp_query;

      $big = 99999999;

      echo paginate_links(array(

          'base' => str_replace($big, '%#%', get_pagenum_link($big)),

          'format' => '?paged=%#%',

          'total' => $wp_query->max_num_pages,

          'current' => max(1, get_query_var('paged')),

          'show_all' => false,

          'end_size' => 2,

          'mid_size' => 3,

          'prev_next' => true,

          'prev_text' => 'Prev',

          'next_text' => 'Next',

          'type' => 'list'

      ));

}



function change_post_menu_label() {

    global $menu;

    global $submenu;

    $menu[5][0] = 'Blog';

    $submenu['edit.php'][5][0] = 'Blog';

    $submenu['edit.php'][10][0] = 'Add Blog Post';

    echo '';

}

function change_post_object_label() {

        global $wp_post_types;

        $labels = &$wp_post_types['post']->labels;

        $labels->name = 'Blog';

        $labels->singular_name = 'Blog';

        $labels->add_new = 'Add Blog';

        $labels->add_new_item = 'Add Blog';

        $labels->edit_item = 'Edit Blog';

        $labels->new_item = 'Blog';

        $labels->view_item = 'View Blog';

        $labels->search_items = 'Search Blog Posts';

        $labels->not_found = 'No Blogs found';

        $labels->not_found_in_trash = 'No Blog Posts found in Trash';

}

add_action( 'init', 'change_post_object_label' );

add_action( 'admin_menu', 'change_post_menu_label' );



function root_relative_url($input) {

  preg_match('|https?://([^/]+)(/.*)|i', $input, $matches);



  if (!isset($matches[1]) || !isset($matches[2])) {

    return $input;

  } elseif (($matches[1] === $_SERVER['SERVER_NAME']) || $matches[1] === $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']) {

    return wp_make_link_relative($input);

  } else {

    return $input;

  }

}



function enable_root_relative_urls() {

  return !(is_admin() || preg_match('/sitemap(_index)?\.xml/', $_SERVER['REQUEST_URI']) || in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php')));

}



if (enable_root_relative_urls()) {

  $root_rel_filters = array(

    'bloginfo_url',

    'the_permalink',

    'wp_list_pages',

    'wp_list_categories',

    'soil_wp_nav_menu_item',

    'the_content_more_link',

    'the_tags',

    'get_pagenum_link',

    'get_comment_link',

    'month_link',

    'day_link',

    'year_link',

    'term_link',

    'the_author_posts_link',

    'script_loader_src',

    'style_loader_src'

  );



  add_filters($root_rel_filters, 'root_relative_url');

}



function add_filters($tags, $function) {

  foreach($tags as $tag) {

    add_filter($tag, $function);

  }

}



if (class_exists('MultiPostThumbnails')) {

    $types = grid_post_types();

    foreach($types as $type) {

        new MultiPostThumbnails(array(

            'label' => 'Grid Image',

            'id' => 'grid-image',

            'post_type' => $type

            )

        );

    }

}



function my_connection_types() {

    p2p_register_connection_type( array(

        'name' => 'project_to_case_study',

        'from' => 'project',

        'to' => 'case_study'

    ) );

}

add_action( 'p2p_init', 'my_connection_types' );



function register_my_menu() {

  register_nav_menu('mega-menu',__( 'Mega Menu' ));
	register_nav_menu('project-menu',__( 'Project Menu' ));
}

add_action( 'init', 'register_my_menu' );



function register_menu_pusher() {

  register_nav_menu('menu-pusher',__( 'Menu Pusher' ));

}

add_action( 'init', 'register_menu_pusher' );