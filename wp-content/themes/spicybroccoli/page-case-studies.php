<?php
/**
 * Template Name: Case Studies
 * 
 * @package Spicy Broccoli
 * @since Spicy Broccoli 1.0
 * @version 1.0
 */

get_header(); ?>
	<div class="bg-white">
		<div class="wrapper cf">

			<?php while ( have_posts() ) : the_post(); ?>
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( 'Permalink to %s', the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

							<div class="what-we-do__grid">
								<?php $projects_query = array(
									'post_type' => 'case_study',
									'posts_per_page' => -1
									);

								$projects = new WP_Query($projects_query);
								?>
								<?php if ($projects->have_posts()): ?>
									<div class="packery">
									<?php while($projects->have_posts()): $projects->the_post(); ?>
										<?php $class = 'grid-item'; ?>
								    	<a href="<?php the_permalink(); ?>" class="<?php echo $class; ?>">
											<?php if (class_exists('MultiPostThumbnails')) :
											    MultiPostThumbnails::the_post_thumbnail(
											        get_post_type(),
											        'grid-image'
											    );
											endif; ?>
											<div class="project-title"><span><?php the_title(); ?></span></div>
								    	</a>
								    <?php endwhile; ?>
								    </div>
								<?php endif; ?>
							</div>

							

							<div class="entry-utility">
								<?php edit_post_link('Edit', '<span class="edit-link">', '</span>' ); ?>
							</div><!-- .entry-utility -->
						</div><!-- #post-## -->


				<?php endwhile; // End the loop. Whew. ?>

		</div><!-- .wrapper -->
	</div><!-- .bg-white-->

	<?php get_template_part('partials/strip-case-studies'); ?>

	<?php get_template_part('partials/strip-contact'); ?>

<?php get_footer(); ?>