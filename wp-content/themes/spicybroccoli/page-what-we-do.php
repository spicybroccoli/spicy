<?php

/**

 * Template Name: What We Do

 * 

 * @package Spicy Broccoli

 * @since Spicy Broccoli 1.0

 * @version 1.0

 */

get_header(); ?>

<div class="bg-white">
  <div class="wrapper cf">
    <?php while ( have_posts() ) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <div class="what-we-do__menu">
        <?php 
		$args = array(
				'hide_empty'	=> false
				);
		$cats = get_terms( 'project_type', $args); ?>
        <?php if ($cats): ?>
        <ul class="project-type">
          <?php foreach($cats as $cat): ?>
          <?php 

				$class = '';

				if ($_GET['project-type'] == $cat->slug || ( $_GET['project-type'] == '' && $cat->slug == 'everything' ) ) {

					$class = 'active';

				}

				?>
          <li><a class="<?php echo $class; ?>" href="<?php echo get_term_link( $cat, 'project_type' ); ?>"><?php echo $cat->name; ?></a></li>
          <?php endforeach; ?>
        </ul>
        <?php endif; ?>
      </div>
      <div class="what-we-do__grid">
        <?php $projects_query = array(

									'post_type' => 'project',

									'posts_per_page' => -1

									);



								if ($_GET['project-type']) {

									$projects_query['project_type'] = $_GET['project-type'];

								}



								$projects = new WP_Query($projects_query);

								?>
        <?php if ($projects->have_posts()): ?>
        <div class="packery">
          <?php while($projects->have_posts()): $projects->the_post(); ?>
          <?php $class = ( get_post_meta(get_the_ID(), '_featured', true) == 1 ) ? 'grid-item w2 h2' : 'grid-item'; ?>
          <a href="<?php the_permalink(); ?>" class="<?php echo $class; ?>">
          <?php if (class_exists('MultiPostThumbnails')) :

											    MultiPostThumbnails::the_post_thumbnail(

											        get_post_type(),

											        'grid-image'

											    );

											endif; ?>
          <div class="project-title"><span>
            <?php the_title(); ?>
            </span></div>
          </a>
          <?php endwhile; ?>
        </div>
        <?php endif; ?>
      </div>
      <div class="entry-utility">
        <?php edit_post_link('Edit', '<span class="edit-link">', '</span>' ); ?>
      </div>
      <!-- .entry-utility --> 
      
    </div>
    <!-- #post-## -->
    
    <?php endwhile; // End the loop. Whew. ?>
  </div>
  <!-- .wrapper --> 
  
</div>
<!-- .bg-white-->

<?php get_template_part('partials/strip-client'); ?>
<?php get_template_part('partials/strip-contact'); ?>
<?php get_footer(); ?>
