<?php
/**
 * The Single template for the Project custom post type
 * 
 * @package Spicy Broccoli
 * @since Spicy Broccoli 1.0
 * @version 1.0
 */

get_header(); ?>


		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
        
<div class="bg-white">
		<div class="wrapper cf">
		<h1><?php the_title(); ?></h1>
        <div class="overview"><?php $project_overview = get_post_meta(get_the_ID(), '_project_overview', true);
			 echo $project_overview; ?></div>
        
        <div class="project-gallery">
         	<ul class="slides">
         	<?php $galleryArray = get_post_gallery_ids($post->ID); ?>
			<?php foreach ($galleryArray as $slide): ?>
				<li>
					<img src="<?php echo wp_get_attachment_url( $slide ); ?>" />
				</li>

          	<?php endforeach; ?>
          </ul>
        </div>
        
        
		</div><!-- .wrapper -->
	</div><!-- .bg-white -->
    
        
    <div class="bg-grey bg-light-grey cf">
		<div class="wrapper">
        <div>
      	<?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div><!-- .entry-summary -->
	<?php else : ?>
			<div class="entry-content project-content">
				<?php the_content( 'Continue reading <span class="meta-nav">&rarr;</span>'); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' .  'Pages:' , 'after' => '</div>' ) ); ?>
			</div><!-- .entry-content -->
	<?php endif; ?>
        </div>
        <div class="project-meta">
       
       <div class="client-logo">
       <?php $client_logo = wp_get_attachment_image_src( get_post_meta(get_the_ID(), '_client_logo', true), 'full' ); ?>
       <?php if ($client_logo): ?>
       <img src="<?php echo $client_logo[0]; ?>" />
       <?php endif; ?>
       </div>
       
       <div class="client-name">
		<?php $client_name = get_post_meta(get_the_ID(), '_client_name', true); echo $client_name; ?><br/>
		<a class="client-url" target="_blank" href="<?php $client_url = get_post_meta(get_the_ID(), '_client_url', true); echo $client_url; ?>" />View website</a>
       </div>
       
       <div class="entry-share">
		<!-- Go to www.addthis.com/dashboard to customize your tools -->
		<div class="addthis_sharing_toolbox"><span>Share it</span></div>
       </div>
       
       	<div style="display:none;" class="related-project related-project-client cf">
			<?php $project_client = wp_get_object_terms( get_the_ID(), 'project_client'); ?>

				<h5>More Projects from <?php echo $project_client[0]->name; ?></h5>
				<?php


				$related_client_projects = array();
				if ( ! empty( $project_client) ) {
					if ( ! is_wp_error( $project_client) ) {
							foreach( $project_client as $term ) {
								$posts = new WP_Query(array('project_client' => $term->slug, 'posts_per_page' => 4, 'post__not_in' => (array) get_the_ID()));
								$related_client_projects[] = $posts->posts;
							}
					}
					array_unique($related_client_projects, SORT_REGULAR);
				

					if ( ! empty( $related_client_projects ) ) {
								 	if ( ! is_wp_error( $related_client_projects ) ) {
								 		echo '<ul>';
								 			foreach( $related_client_projects[0] as $project ) {
								 				
								 				echo '<li><a href="' . get_permalink( $project->ID ) . '">' . $project->post_title . '</a></li>'; 
								 			}
								 		echo '</ul>';
								 	}
								 }
				}
				wp_reset_postdata();

			?>
       	</div>
       	<div class="related-project related-project-project_type cf">
       	<h5>Related Projects</h5>
       		<?php
       		// Cherry picked related projects
       		// Filter out spaces, transform to array and display
       		// Else default to the project_type
       		$related_projects = get_post_meta(get_the_ID(), 'related_projects', true);

       		if ($related_projects) {
       			$related_projects =  explode(',', str_replace(' ', '', $related_projects));
       			$related_projects = array_rand(array_unique($related_projects, SORT_NUMERIC), 2); // Pick 2 projects randomly
       			foreach ($related_projects as $project_id) {
       				echo get_the_title($project_id);
       			}

       		} else {
	       		$project_type = array_map(function($item){
	       			return $item->slug;
	       		}, (array) wp_get_object_terms( get_the_ID(), 'project_type'));

	       		$related_project__project_type = new WP_Query(
	       			array(
	       				'post__not_in' => (array) get_the_ID(), 
	       				'posts_per_page' => 2, 
	       				'post_type' => 'project', 
	       				'tax_query' => 
	       					array(
	       						'taxonomy' => 'project_type', 
	       						'field' => 'slug', 
	       						'terms' => $project_type
	       					)
	       				)
	       			);

	       		if($related_project__project_type->have_posts()): 
	       			echo '<ul>';
	       			while($related_project__project_type->have_posts()): $related_project__project_type->the_post();
	       				echo '<li><a href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a></li>'; 
					endwhile;
			 		echo '</ul>';
	       		endif;
       		}
       		wp_reset_postdata();
       		


       		?>
       	</div>

        </div>
        
			
            
		</div><!-- .wrapper -->
	</div><!-- .bg-grey -->
    
    <?php $client_testimonial = get_post_meta(get_the_ID(), '_client_testimonial', true); ?>
    <?php $client_testimonial_name = get_post_meta(get_the_ID(), '_client_testimonial_name', true); ?>
	<?php if ( $client_testimonial ): ?>
      <div class="bg-white">
		<div class="wrapper cf">
        <div class="testimonial"><?php echo $client_testimonial; ?></div>
        <div class="testimonial-name"><?php echo $client_testimonial_name; ?></div>
        
		</div><!-- .wrapper -->
	</div><!-- .bg-white -->
	<?php endif; ?>

	<?php get_template_part('partials/strip-projects'); ?>

	<?php get_template_part('partials/strip-contact'); ?>

<?php get_footer(); ?>