<?php



/**



 * The template for displaying the footer.



 * 



 * @package Spicy Broccoli



 * @since Spicy Broccoli 1.0



 * @version 1.0



 */



?>

</div>

<!-- #main -->



<div id="social">

  <div class="wrapper">

    <?php wp_nav_menu( array( 'theme_location' => 'social-nav' ) ); ?>

  </div>

  <!-- .wrapper --> 

  

</div>

<!-- #social -->



<div id="footer" role="contentinfo">

  <div id="colophon" class="wrapper"> <a id="logo-footer" href="<?php echo site_url( '/' ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_sbm_memorable.png"></a>

    <hr />

    <div class="sbm">

    &copy; <?php echo date("Y"); ?> Spicy Broccoli Media | Graphic Design Sydney</div>

    <?php wp_nav_menu( array( 'theme_location' => 'footer-nav' ) ); ?>

  </div>

  <!-- #colophon --> 

  

</div>

<!-- #footer -->







<?php wp_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.js"></script>
<script> new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) ); </script>


</body></html>