<?php
/**
 * 404 Error template
 * 
 * @package Spicy Broccoli
 * @since Spicy Broccoli 1.0
 * @version 1.0
 */
get_header(); ?>

	<div class="bg-white">
		<div class="wrapper cf">
			<!--<div id="content" role="main">-->
			<div class="seven-col fl">

				<div id="post-0" class="post error404 not-found">
					<h1 class="entry-title">Not Found</h1>
					<div class="entry-content">
						<p>Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.</p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-0 -->
				
			</div><!-- #content -->
			<?php get_sidebar(); ?>
		</div><!-- .wrapper -->
	</div><!-- .bg-white-->

<?php get_footer(); ?>