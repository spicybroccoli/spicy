<?php

// Defined Routes
// Non Standard

include('lib/hm-rewrite.php');

hm_add_rewrite_rule( array(
 
    'regex'     => '^blog(?:\/page\/([0-9]+))?$',
    'query'     => 'index.php?post_type=post&paged=$matches[1]',
    'query_callback' => function( WP_Query $query ) {
 
        //overwrite is_home because WordPress gets it wrong here
        $query->is_home = false;
        $query->is_404 = false;
        $query->is_archive = true;
        $query->is_post_type_archive = true;
        $query->is_posts_page = true;
    },

));

hm_add_rewrite_rule( array(
 
    'regex'     => '^blog/tag/([^/]+)?', 
    'query'     => 'index.php?post_type=post&tag=$matches[1]',
    'query_callback' => function( WP_Query $query ) {
 
        //overwrite is_home because WordPress gets it wrong here
        $query->is_home = false;
        $query->is_404 = false;
        $query->is_archive = true;
        $query->is_tag = true;
        $query->is_tax = true;

       
    },

));


hm_add_rewrite_rule( array(
 
    'regex'     => '^blog/([0-9]{4})/([0-9]{1,2})?', 
    'query'     => 'index.php?post_type=post&year=$matches[1]&monthnum=$matches[2]',
    'query_callback' => function( WP_Query $query ) {
 
        //overwrite is_home because WordPress gets it wrong here
        $query->is_home = false;
        $query->is_404 = false;
        $query->is_archive = true;
  		$query->is_date = true;
  		$query->is_month = true;

       
    },

));

hm_add_rewrite_rule( array(
 
    'regex'     => '^contact/submit$', 
    'template' => 'inc/contact-submission.php'

));

add_filter('term_link', function($termlink, $term, $taxonomy){
	$return = $termlink;
	if ( $taxonomy == 'project_type' ) {
		if ($term->slug == 'everything') {
			$return = site_url('/what-we-do');
		} else {
			$return = site_url('/what-we-do/' . $term->slug );
		}
	}
	return $return;
}, 10, 3);


hm_add_rewrite_rule( array(
 
    'regex'     => '^what-we-do(?:\/([0-9a-zA-z\-]+))?$', // a review category page
    'query'     => 'index.php?post_type=project&project_type=$matches[1]&',
    'template'    => 'what-we-do.php',
    'request_callback' => function( WP $wp ) {
    	  $wp->query_vars['posts_per_page'] = -1;
    },
    'query_callback' => function( WP_Query $query ) {
 
        //overwrite is_home because WordPress gets it wrong here
        $query->is_home = true;
        $query->is_404 = false;
        $query->is_archive = true;
        $query->is_post_type_archive = true;
        
    },
    'title_callback' => function( $title, $seperator ) {
        return 'What we do' . ' ' . $seperator . ' ' . $title;
     }

));


?>
