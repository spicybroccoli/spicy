<?php
/**
 * Load Our Scripts
 *
 */

// Load Our Responsive Nav Scripts
add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_style('responsive-nav-css', get_stylesheet_directory_uri() .'/css/responsive-nav/responsive-nav.css');
	
	if (is_singular('project') || is_singular('case-study') || is_post_type_archive('project') || is_post_type_archive('case-study')) {
		wp_enqueue_style('jquery.flexslider', get_stylesheet_directory_uri() .'/js/flexslider.css');
		wp_enqueue_script('jquery.flexslider', get_stylesheet_directory_uri() . '/js/jquery.flexslider-min.js', array('jquery'), 1.0, true);
	}
	
	wp_enqueue_script('jquery', array(), 1.0, true);
	
	wp_enqueue_script('classie', get_stylesheet_directory_uri() . '/js/responsive-nav/classie.js', array(), 1.0, true);
	wp_enqueue_script('mlpushmenu', get_stylesheet_directory_uri() . '/js/responsive-nav/mlpushmenu.js', array(), 1.0, true);

	wp_enqueue_script('shuffle', get_stylesheet_directory_uri() . '/js/packery.pkgd.min.js', array('jquery'));

	
	
	wp_enqueue_script('parsley', get_stylesheet_directory_uri() . '/js/parsley.min.js', array(), 1.0, true);
	wp_enqueue_script('site', get_stylesheet_directory_uri() . '/js/site.js', array(), 1.0, true );

});

add_action( 'admin_enqueue_scripts', function() {
    wp_enqueue_style( 'admin.css', get_stylesheet_directory_uri() . '/inc/admin.css' );
});  