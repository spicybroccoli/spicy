<?php

class SBM_Generic_Metabox {

    public function __construct() {
            add_action( 'post_submitbox_misc_actions', array($this, 'render_metabox'), 10, 1 );
            add_action( 'save_post', array( $this, 'save_postdata' ), 10, 1 );
        }

        /**
         * Save the meta when the post is saved.
         *
         * @param int $post_id The ID of the post being saved.
         */
        function save_postdata($post_id) {   
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return false;
            if ( !current_user_can( 'edit_page', $postid ) ) return false;


            update_post_meta($post_id, '_show_on_homepage', $_POST['show_on_homepage']);
            update_post_meta($post_id, '_featured', $_POST['featured']);
        }


        function render_metabox() {
            global $post;

            $homepage = get_post_meta($post->ID, '_show_on_homepage', true);
            $featured = get_post_meta($post->ID, '_featured', true);

            echo '<div class="misc-pub-section misc-pub-section-last">
                 <span id="timestamp">'
                 . '<label><input type="checkbox"' . (!empty($homepage) ? ' checked="checked" ' : null) . 'value="1" name="show_on_homepage" /> Show on Homepage</label>'
            .'</span></div>';
            echo '<div class="misc-pub-section misc-pub-section-last"><span id="timestamp"><label><input type="checkbox"' . (!empty($featured) ? ' checked="checked" ' : null) . 'value="1" name="featured" /> Feature Post</label></span></div>';
        }

}

new SBM_Generic_Metabox();