<?php
function get_the_breadcrumbs() {
    // Get argument
    if (func_num_args > 0) {
    $input = func_get_arg(0);
    }
    // Assume global $post object if not set
    if (empty($input)) {
        global $post;
        $input = $post;
    }
    // Get variable type
    $type = gettype($input);
    // Has a post object or post id been passed?
    switch ($type) {
        case 'integer':
            $post_object = get_post($input);
            break;
        case 'object':
            $post_object = $input;
            break;
    }
    $temp = $post;
    $post = $post_object;
    setup_postdata($post);
    // Lets get the ancestors
    $ancestors = get_post_ancestors($post_object);
    $ordered_ancestors = array_reverse($ancestors);
    // Okay let's build the Breadcrumbs
    $ancestor_count = count($ancestors);
    $crumbs .= '<ul class="breadcrumbs clearfix">';
    $crumbs .= '<li class="home page"><a href="'. site_url('/') . '">Home</a>';

    // singular, tax archive or cpt archive

    if ( is_archive() ) {
        $queried_object = get_queried_object();

        if ( is_object($queried_object) ) {

            // Lets do some hacky introspection
            if ( property_exists($queried_object, 'labels') ) {
                // We've got a CPT object
                $crumbs .= '<li class="home page"><a href="#">'. $queried_object->labels->name .'</a>';
            } else if ( property_exists($queried_object, 'term_id') ) {
                // We've got a tax term 
                $crumbs .= '<li class="home page"><a href="#">'. $queried_object->name .'</a>';
            } else {
                // We've probably got a Date archive, but not sure :/
            
            }

        } else {
            $year = get_query_var('year');
            $monthnum = get_query_var('monthnum');
            if ($year && $monthnum) {
                $crumbs .= '<li class="home page"><a href="'. site_url('/blog') . '">Blog</a>';
                $crumbs .= '<li class="home page"><a href="' . get_month_link( $year, $monthnum ) . '">' . trim(single_month_title(' ', false)) . '</a></li>';
            }
        }
        
    } else if ( is_singular() ) {

        // Output the post type
        $post_type_obj = get_post_type_object( get_post_type() );


        if ( get_post_type() == 'page' ) {

            foreach ($ancestors as $ancestor) {
                if (count($ancestors) == $ancestor_count) {
                    $class = 'first-parent page';
                } else {
                    $class = 'parent page';
                }
                    $parent_post = array_pop($ancestors);
                    $crumbs .= '<li class="'. $class.'"><a href="'. get_permalink($ancestor) . '">'. get_post($ancestor)->post_title . '</a></li>';
            }
        } else {
            $archive_link = get_post_type_archive_link( $post->post_type ) ? get_post_type_archive_link( $post->post_type ) : site_url('/blog');
            $crumbs .= '<li class="home page"><a href="'. $archive_link . '">'. $post_type_obj->labels->name .'</a>';
        }

        $crumbs .= '<li class="'. $class.'"><a href="'. get_permalink($post->ID) . '">'. get_post($post->ID)->post_title . '</a></li>';
    }


    $crumbs .= '</ul>';
    // Okay let's return it
        return $crumbs;
 }
// Output the Menu 
function the_breadcrumbs() {
    // If we have arguments send them through
    if (func_num_args > 0) {
    $input = func_get_arg(0);
    echo get_the_breadcrumbs($input);
    // Else we'll just use the global $post object
    } else {
        echo get_the_breadcrumbs();
    }
}