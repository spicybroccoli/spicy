<?php

if ( ! isset( $content_width ) )
	$content_width = 1100;

/** Tell WordPress to run twentyten_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', function() {

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
	add_theme_support( 'title-tag' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => 'Primary Navigation',
		'top-nav' => 'Top Navigation',
		'footer-nav' => 'Footer Navigation',
		'social-nav' => 'Social Navigation',
	));

    register_sidebar(array(
        'name' => 'Sidebar',
        'id' => 'sidebar',
        'description' => 'Widgets in this area will be shown on all posts and pages.',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

});