<?php
/**
 * Load Our Scripts
 *
 */

class SBM_Branding {

	function __construct() {
		$this->admin_branding();
		$this->dashboard_widget_config();
		$this->disable_unused_query_vars();
		$this->disable_admin_menu_pages();
		$this->disable_update_nag();
		$this->clean_up_header();
	}

	private function admin_branding() {
		add_action('login_head', function() {
			
			echo '<style type="text/css">';
				echo 'h1 a { background-image: url('. get_stylesheet_directory_uri() .'/images/logo_sbm.png) !important;';
				echo 'height:75px !important;';
				echo 'width: 185px !important;';
				echo 'background-size: auto !important;}';
				echo '#loginform input[type=text], #loginform input[type=password] { padding: 10px !important; }';
			echo '</style>';
			
		});

		add_filter('login_headerurl', function() {
			    return home_url('/');
		});

		add_filter('login_headertitle', function() {
			return get_option('blogname');
		});
		
		add_filter('admin_footer_text', function() {
 			return '&copy; '. date('Y') . ' - Spicy Broccoli Media: Good for your Company\'s Health';
		});

	}

	private function clean_up_header() {
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'feed_links_extra', 3);
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	}

	private function dashboard_widget_config() {
		add_action('wp_dashboard_setup', function() {
			global $wp_meta_boxes;
			unset($wp_meta_boxes['dashboard']['normal']['high']['dashboard_browser_nag']);
			// $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		});
	}


	private function disable_unused_query_vars() {
		add_action('pre_get_posts', function($query) {
			if ($query->is_date || $query->is_year || $query->is_month || $query->is_time || $query->is_author || $query->is_category || $query->is_tag || $query->is_comment_feed || $query->is_trackback ||$query->is_comments_popup || $query->is_attachment) {

					$query->is_404 = true;
			}
		}, 10, 1);
	}

	private function disable_admin_menu_pages() {
		add_action( 'admin_menu', function() {
    		if (!current_user_can('manage_options')) {
		    	remove_menu_page('edit-comments.php');
		    	remove_menu_page('tools.php');
		    	remove_menu_page('edit.php');
		    	remove_menu_page('themes.php');
    		}
		} , 99999999999);
	}

	private function disable_update_nag() {
		add_action('admin_notices', function() {
			if ( !current_user_can('activate_plugins') ) {
				remove_action('admin_notices', 'update_nag', 3);
			}
		});
	}

}


new SBM_Branding();