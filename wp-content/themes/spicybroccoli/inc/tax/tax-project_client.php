<?php
add_action( 'init', 'register_taxonomy_project_clients' );

function register_taxonomy_project_clients() {

    $labels = array( 
        'name' => _x( 'Project Clients', 'project_clients' ),
        'singular_name' => _x( 'Project Client', 'project_clients' ),
        'search_items' => _x( 'Search Project Clients', 'project_clients' ),
        'popular_items' => _x( 'Popular Project Clients', 'project_clients' ),
        'all_items' => _x( 'All Project Clients', 'project_clients' ),
        'parent_item' => _x( 'Parent Project Client', 'project_clients' ),
        'parent_item_colon' => _x( 'Parent Project Client:', 'project_clients' ),
        'edit_item' => _x( 'Edit Project Client', 'project_clients' ),
        'update_item' => _x( 'Update Project Client', 'project_clients' ),
        'add_new_item' => _x( 'Add New Project Client', 'project_clients' ),
        'new_item_name' => _x( 'New Project Client', 'project_clients' ),
        'separate_items_with_commas' => _x( 'Separate project clients with commas', 'project_clients' ),
        'add_or_remove_items' => _x( 'Add or remove Project Clients', 'project_clients' ),
        'choose_from_most_used' => _x( 'Choose from most used Project Clients', 'project_clients' ),
        'menu_name' => _x( 'Project Clients', 'project_clients' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_admin_column' => false,
        'hierarchical' => false,

        'rewrite' => array('with_front' => false, 'slug' => 'client'),
        'query_var' => true
    );

    register_taxonomy( 'project_client', array('project'), $args );
}