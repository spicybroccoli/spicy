<?php
add_action( 'init', 'register_taxonomy_project_type' );

function register_taxonomy_project_type() {

    $labels = array( 
        'name' => _x( 'Project Type', 'project_type' ),
        'singular_name' => _x( 'Project Types', 'project_type' ),
        'search_items' => _x( 'Search Project Type', 'project_type' ),
        'popular_items' => _x( 'Popular Project Type', 'project_type' ),
        'all_items' => _x( 'All Project Type', 'project_type' ),
        'parent_item' => _x( 'Parent Project Types', 'project_type' ),
        'parent_item_colon' => _x( 'Parent Project Types:', 'project_type' ),
        'edit_item' => _x( 'Edit Project Types', 'project_type' ),
        'update_item' => _x( 'Update Project Types', 'project_type' ),
        'add_new_item' => _x( 'Add New Project Types', 'project_type' ),
        'new_item_name' => _x( 'New Project Types', 'project_type' ),
        'separate_items_with_commas' => _x( 'Separate project type with commas', 'project_type' ),
        'add_or_remove_items' => _x( 'Add or remove Project Type', 'project_type' ),
        'choose_from_most_used' => _x( 'Choose from most used Project Type', 'project_type' ),
        'menu_name' => _x( 'Project Type', 'project_type' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_admin_column' => false,
        'hierarchical' => false,

        'rewrite' => true,
        'query_var' => true
    );

    register_taxonomy( 'project_type', array('project'), $args );
}