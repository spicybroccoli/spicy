<?php

class WorkflowMax_API {

	function __construct($api_key, $account_key) {
		$this->api_key = $api_key;
		$this->account_key = $account_key;
	}

	private function workflowMaxXML($verb, $endpoint, $message) {
	    $ch = curl_init();
	    curl_setopt( $ch, CURLOPT_URL, 'https://api.workflowmax.com/' . $endpoint . '?apiKey=' . $this->api_key . '&accountKey=' . $this->account_key );
	    curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $verb);
	    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt( $ch, CURLOPT_POSTFIELDS, $message );
	    $result = curl_exec($ch);
	    curl_close($ch);

	    return simplexml_load_string($result);
	}

	function createProspect($name, $email, $phone, $referral) {
	  $endpoint = 'client.api/add';
	  $data = '<Client><Name>'.$name.'</Name><Email>'.$email.'</Email><Phone>'.$phone.'</Phone><ReferralSource>'.$referral.'</ReferralSource><IsProspect>Yes</IsProspect><Contacts><Contact><Name>'.$name.'</Name><Email>'.$email.'</Email><Phone>'.$phone.'</Phone></Contact></Contacts></Client>';

	  return $this->workflowMaxXML('POST', $endpoint, $data);

	}

	function createLead($clientID, $name, $message) {
	  $endpoint = 'lead.api/add';
	  $ID = $clientID; 
	  $data = "<Lead><Name>".$name."</Name><Description>".$message."</Description><ClientID>".$ID."</ClientID><OwnerID>173259</OwnerID><EstimatedValue>0</EstimatedValue><CategoryID>17775</CategoryID><TemplateID>5243</TemplateID></Lead>";

	  return $this->workflowMaxXML('POST', $endpoint, $data);
	}

	function updateLead($leadID, $fields) {

	  $endpoint = 'lead.api/update/'.$leadID.'/customfield';
	  $data = '<CustomFields>';
	  foreach($fields as $field) {
	  	$data .= '<CustomField><ID>' . $field['ID'] . '</ID><Text>' . $field['value'] . '</Text></CustomField>';
	  }
	  $data .= '</CustomFields>';

	  return $this->workflowMaxXML('PUT', $endpoint, $data);
	}
}

?>