<?php
// Default Utilities

/**
 * Sets the post excerpt length to 40 characters.
 *
 * @return int
 */
function sbm_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'sbm_excerpt_length' );


/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @return string "Continue Reading" link
 */
function sbm_continue_reading_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyten' ) . '</a>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and twentyten_continue_reading_link().
 *
 * @return string An ellipsis
 */
function sbm_auto_excerpt_more( $more ) {
	return '';
}
add_filter( 'excerpt_more', 'sbm_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function sbm_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= sbm_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'sbm_custom_excerpt_more' );

/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function sbm_posted_on() {
	printf( __( '<span class="%1$s">Posted on</span> %2$s <span class="meta-sep">by</span> %3$s', 'twentyten' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			sprintf( esc_attr__( 'View all posts by %s', 'twentyten' ), get_the_author() ),
			get_the_author()
		)
	);
}
