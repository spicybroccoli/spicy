<?php

class SBM_Case_Study_Metabox {

    public function __construct() {
            add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
            add_action( 'save_post', array( $this, 'save' ) );
   
        }

        /**
         * Adds the meta box container.
         */
        public function add_meta_box( $post_type ) {
            add_meta_box( 'casestudymetadiv', 'Case Study Details', array( $this, 'render_meta_box_content' ), 'case_study', 'advanced', 'high');
                
        }

        /**
         * Save the meta when the post is saved.
         *
         * @param int $post_id The ID of the post being saved.
         */
        public function save( $post_id ) {
        
            /*
             * We need to verify this came from the our screen and with proper authorization,
             * because save_post can be triggered at other times.
             */

            // Check if our nonce is set.
            if ( ! isset( $_POST['sbm_case_study_nonce'] ) )
                return $post_id;

            $nonce = $_POST['sbm_case_study_nonce'];

            // Verify that the nonce is valid.
            if ( ! wp_verify_nonce( $nonce, 'sbm_case_study' ) )
                return $post_id;

            // If this is an autosave, our form has not been submitted,
                    //     so we don't want to do anything.
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
                return $post_id;

            // Check the user's permissions.
            if ( 'case_study' == $_POST['post_type'] ) {

                if ( ! current_user_can( 'edit_page', $post_id ) )
                    return $post_id;
        
            } else {

                if ( ! current_user_can( 'edit_post', $post_id ) )
                    return $post_id;
            }

            /* OK, its safe for us to save the data now. */

            // Sanitize the user input.



            // Update the meta field.
      
            update_post_meta( $post_id, '_cs_before', sanitize_text_field( $_POST['cs_before']) );
            update_post_meta( $post_id, '_cs_before', sanitize_text_field( $_POST['cs_before']) );
    

        }


        /**
         * Render Meta Box content.
         *
         * @param WP_Post $post The post object.
         */
        public function render_meta_box_content( $post ) {

            // Enqueue media scripts
            //wp_enqueue_scripts();
        
            // Add an nonce field so we can check for it later.
            wp_nonce_field( 'sbm_case_study', 'sbm_case_study_nonce' );

            // Display the form, using the current value.
 


            $this->textarea_field(array('key' => 'cs_before', 'heading' => 'Case Study Before'));

            $this->textarea_field(array('key' => 'cs_after', 'heading' => 'Case Study After'));







            
       }



       private function autocomplete_field( $opts ) {

          global $post;
          $key = sanitize_title($opts['key']); 
          $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 

          

          ?>

          <div class="row row-<?php echo $key; ?>">
            <label for="<?php echo $key; ?>">
              <?php echo $opts['heading']; ?>
            </label>
           <ul id="myTags">
               <!-- Existing list items will be pre-added to the tags -->
               <li>Tag1</li>
               <li>Tag2</li>
           </ul>
          </div>

    

        <?php
          // Reset $value
          $value = null;
        }

      private function input_field( $opts ) { ?>
        <?php 

        global $post;
        $key = sanitize_title($opts['key']); 
        $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 

        ?>

        <div class="row row-<?php echo $key; ?>">
          <label for="<?php echo $key; ?>">
            <?php echo $opts['heading']; ?>
          </label>
          <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value=" <?php echo esc_attr( $value ); ?>" size="25" />
        </div>

      <?php
        // Reset $value
        $value = null;
      }

      private function textarea_field ( $opts ) { ?>
        <?php 

        global $post;
        $key = sanitize_title($opts['key']); 
        $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 

        ?>

        <div class="row row-<?php echo $key; ?>">
          <label for="<?php echo $key; ?>">
            <?php echo $opts['heading']; ?>
          </label>
          <?php wp_editor($value, $key); ?>
        </div>

      <?php 
        // Reset $value
        $value = null;

      }

     private function upload_field( $opts ) {
        global $post;
        $defaults = array(
            'heading' => 'Image',
            'key' => 'image',
            'size' => 'thumbnail'
        );
        $opts = wp_parse_args( $opts, $defaults );
        $key = sanitize_title($opts['key']);
        $attach_id = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 


        $thumbnail_url = wp_get_attachment_image_src( $attach_id, $opts['size'] );

        $thumbnail = ($thumbnail_url[0]) ? $thumbnail_url[0] : includes_url('/images/media/document.png');

        $filename = (get_attached_file($attach_id)) ? basename(get_attached_file($attach_id)) : 'No Image Selected';

        ?>

        <div class="row row-<?php echo $key; ?>">
        <label for="<?php echo $key; ?>"><?php echo $opts['heading']; ?></label>
          <div id="media-items" class="hide-if-no-js">
            <div id="media-item-<?php echo $key; ?>" class="media-item child-of-0 open">
              <img class="pinkynail" src="<?php echo $thumbnail; ?>" alt="">
              <button class="button edit-attachment select-<?php echo $key; ?>">Select Image</button>
              <input type="hidden" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo $attach_id; ?>" />
              <div class="filename new"><span class="title"><strong><?php echo $filename; ?></strong></span></div>
            </div>
          </div>
        </div>

        <script>
          jQuery(function(){
            (function() {
                jQuery('.select-<?php echo $key; ?>').on('click', function(e){
                    window.self = this;
                    e.preventDefault();
               
                    // If the media frame already exists, reopen it.
                    if ( modal ) {
                      modal.open();
                      return;
                    }
                });

               var modal = wp.media({
                    title: 'Choose <?php echo $opts["label"]; ?>',
                    frame: 'select',
                    button: {
                              text: 'Select <?php echo $opts["label"]; ?>'
                          },
                          multiple: false,
                });

                modal.on( 'select', function() {
                  // We set multiple to false so only get one image from the uploader
                  attachment = modal.state().get('selection').first().toJSON();
                  
                  jQuery('#<?php echo $key; ?>').val(attachment.id);
                  jQuery('.row-<?php echo $key; ?> span.title strong').text(attachment.filename);
                  jQuery('#media-item-<?php echo $key; ?> .pinkynail').attr('src', attachment.url)
                });

             })();
          });
        </script>

        <?php
      }



}

new SBM_Case_Study_Metabox();