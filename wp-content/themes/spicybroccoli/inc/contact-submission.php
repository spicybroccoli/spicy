<?php

// Utility function to detect if AJAX
function is_ajax() {
	$ret = false;
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$ret = true;
	}
	return $ret;
}


// Lets look at the incoming data



if ($_POST) {

	$fname = filter_input(INPUT_POST, 'FIRST_NAME', FILTER_SANITIZE_SPECIAL_CHARS);
    $lname = filter_input(INPUT_POST, 'LAST_NAME', FILTER_SANITIZE_SPECIAL_CHARS);
    $name = $fname . ' ' . $lname;
	$email = filter_input(INPUT_POST, 'EMAIL', FILTER_SANITIZE_SPECIAL_CHARS);
    $phone = filter_input(INPUT_POST, 'PHONE', FILTER_SANITIZE_SPECIAL_CHARS);
    $message = filter_input(INPUT_POST, 'NOTE', FILTER_SANITIZE_SPECIAL_CHARS);
    $enquiry_type = filter_input(INPUT_POST, 'ENQUIRY_TYPE', FILTER_SANITIZE_SPECIAL_CHARS);
    $referral = filter_input(INPUT_POST, 'REFERRAL', FILTER_SANITIZE_SPECIAL_CHARS);

    $isValidated = true;
    $messages = array();

    if (empty($fname)) {
        $isValidated = false;
        $messages[] = "The First Name field is mandatory";
    }

    if (empty($lname)) {
        $isValidated = false;
        $messages[] = "The Last Name field is mandatory";
    }
				  
    if (empty($email)) {
        $isValidated = false;
        $messages[] = "The Email field is mandatory";
    }

    if (empty($phone)) {
        $isValidated = false;
        $messages[] = "The Phone field is mandatory";
    }

    if (empty($message)) {
        $isValidated = false;
        $messages[] = "The Note field is mandatory";
    }

    if (empty($enquiry_type)) {
        $isValidated = false;
        $messages[] = "The Enquiry Type field is mandatory";
    }

    if ( $isValidated ) {

    	include('workflow-max-api-client.php');
    	$workflow = new WorkflowMax_API('14C10292983D48CE86E1AA1FE0F8DDFE', '35DC8961A43C4E21A7EB40F9C6F23FD7'); 
                      
       	/*
       	$client = $workflow->createProspect($name, $email, $phone, $referral);
        $lead = $workflow->createLead($client->Client->ID, $name, $message);
        $updatedLead = $workflow->updateLead($lead->Lead->ID, array(
        	array(
        		'ID' => '67742',
        		'value' => $enquiry_type
        	),
        	array(
        		'ID' => '67743',
        		'value' => $referral
        	),
        	array(
        		'ID' => '79529',
        		'value' => $_SERVER['HTTP_REFERER'],
        	)
        	)
        );
        */
       
        $email_message = 'Hi Spicy, you have a new lead from your website' . PHP_EOL . PHP_EOL;
        $email_message .= 'First Name: ' . $fname . PHP_EOL;
        $email_message .= 'Last Name: ' . $lname . PHP_EOL;
        $email_message .= 'Email: ' . $email . PHP_EOL;
        $email_message .= 'Phone: ' . $phone . PHP_EOL;
        $email_message .= 'Message: ' . $message . PHP_EOL;
        $email_message .= PHP_EOL . 'You can view this lead in workflow here -> <https://my.workflowmax.com/lead/view.aspx?id='.$lead->Lead->ID.'>';


        $res = wp_mail( 'adrian@spicybroccoli.com', 'New Lead from Spicy Website', $email_message); 

        $client_message = 'Hi ' . $fname .', thanks for getting in touch with Spicy Broccoli.' . PHP_EOL . PHP_EOL;
        $client_message .= 'Here is a summary of the information you sent through: ' . PHP_EOL;
        $client_message .= 'First Name: ' . $fname . PHP_EOL;
        $client_message .= 'Last Name: ' . $lname . PHP_EOL;
        $client_message .= 'Email: ' . $email . PHP_EOL;
        $client_message .= 'Phone: ' . $phone . PHP_EOL;
        $client_message .= 'Message: ' . $message . PHP_EOL;

        $res_client = wp_mail( $email, $fname . ', you just submitted and enquiry on Spicy Broccoli', $client_message); 
    }

}

// Lets prepare our content and detect if this is an AJAX Request.

if( is_ajax() ) {
	/* special ajax here */
	if ( $isValidated ) {
		wp_send_json_success(array('html' => "<h2>We'll be in touch!</h2><div class='entry-content'><p>Thanks for your enquiry, we'll be in touch shortly!</p></div>"));
	} else {
		wp_send_json_error();
	}

} else {
	get_header();
	?>
	<div class="bg-white">
		<div class="wrapper cf">
			<div class="page type-page status-publish hentry">
				<h2>We'll be in touch!</h2>

				<div class="entry-content">
					<p>Thanks for your enquiry, we'll be in touch shortly!</p>
				</div>
			</div>
		</div>
	</div>
	<?php
	get_footer();
}