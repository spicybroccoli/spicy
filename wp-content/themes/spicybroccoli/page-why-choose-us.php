<?php get_header(); ?>
<div class="bg-page">
		<div class="wrapper cf">
			<?php while ( have_posts() ) : the_post(); ?>
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							
						</div><!-- #post-## -->


				<?php endwhile; // End the loop. Whew. ?>

		</div><!-- .wrapper -->
	</div><!-- .bg-white-->

	<?php get_template_part('partials/strip-case-studies'); ?>

	<?php get_template_part('partials/strip-contact'); ?>

<?php get_footer(); ?>