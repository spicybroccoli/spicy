<?php
/**
 * The default Page template
 * 
 * @package Spicy Broccoli
 * @since Spicy Broccoli 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="bg-white">
  <div class="wrapper cf">
    <div class="free-stuff" style="background:none;padding:1em 0;">
      <div class="wrapper">
        <h2>Free Stuff</h2>
        <h5>Discover the seven <span>hot tips</span> to making your business stand out from the competition.</h5>
        <p>Get one hot tip delivered weekly to your inbox.</br>
          Just fill in the form below. <strong>We don't spam.</strong></p>
        <?php get_template_part('partials/mc-subscribe'); ?>
      </div>
      <!-- .wrapper --> 
    </div>
    <!-- .free-stuff --> 
  </div>
  <!-- .wrapper --> 
</div>
<!-- .bg-white-->

<?php get_template_part('partials/strip-case-studies'); ?>
<?php get_template_part('partials/strip-contact'); ?>
<?php get_footer(); ?>
