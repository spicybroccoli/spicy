<?php
/**
 * Template Name: Contact Page
 * 
 * @package Spicy Broccoli
 * @since Spicy Broccoli 1.0
 * @version 1.0
 */

get_header(); ?>
	<div class="bg-white">
		<div class="wrapper cf">

			<?php while ( have_posts() ) : the_post(); ?>
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( 'Permalink to %s', the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

							<div class="entry-content">
								<?php the_content( 'Continue reading <span class="meta-nav">&rarr;</span>'); ?>
								<?php wp_link_pages( array( 'before' => '<div class="page-link">' .  'Pages:' , 'after' => '</div>' ) ); ?>
							</div><!-- .entry-content -->

							<div class="entry-utility">
								<?php edit_post_link('Edit', '<span class="edit-link">', '</span>' ); ?>
							</div><!-- .entry-utility -->
						</div><!-- #post-## -->


				<?php endwhile; // End the loop. Whew. ?>

		</div><!-- .wrapper -->
	</div><!-- .bg-white-->

	<div class="bg-light-grey">
		<div class="wrapper cf">
		<div class="span7 col map-container">
			<div id="map-canvas" style="position: relative; width: 645px; height: 400px"></div>
		</div>
		<div class="span5 offset1 col">
			<div class="address">
				<div class="vcard">
					<h2 class="fn">Spicy Broccoli Media</h2>
					<p class="adr">
						<span class="street-address">1/71 Kenneth Rd</span><br>
						<span class="region">Balgowlah, NSW</span> <span class="postal-code">2093<br>
						<span class="country-name">Australia</span>
					</p>
					<p>Phone: <span class="tel">+61 2 8084 5554</span></p>
					<p>Email: <span class="email"><a href="mailto:info@spicybroccoli.com">info@spicybroccoli.com</a></p>
				</div>

			</div>
		</div>
		</div><!-- .wrapper -->
	</div><!-- .bg-grey -->


	<?php get_template_part('partials/strip-contact'); ?>

	<script type="text/javascript"
	  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQi6nxQNoyMapO15WIcmNm-tNideis6m4">
	</script>
	<script type="text/javascript">
	  function initialize() {
	    var mapOptions = {
	      center: new google.maps.LatLng(-33.787087, 151.268534),
	      zoom: 15,
	      disableDefaultUI: true,
	      overviewMapControl: false
	    };
	    var map = new google.maps.Map(document.getElementById('map-canvas'),
	        mapOptions);
	    var iconBase = '<?php echo get_stylesheet_directory_uri(); ?>/images/';
	    var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(-33.787087, 151.268534),
	      map: map,
	       animation: google.maps.Animation.DROP,
	      icon: iconBase + 'broccoli.png',
	      url: 'https://www.google.com.au/maps/place/Spicy+Broccoli+Media/@-33.787186,151.26848,17z'
	    });
	    google.maps.event.addListener(marker, 'click', function() {window.open(marker.url, '_blank');});
	  }
	  google.maps.event.addDomListener(window, 'load', initialize);
	</script>

<?php get_footer(); ?>