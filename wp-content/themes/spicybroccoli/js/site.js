// http://packery.metafizzy.co/packery.pkgd.js added as external resource

var app = app || {};

// overwrite Packery methods
var __resetLayout = Packery.prototype._resetLayout;
Packery.prototype._resetLayout = function() {
  __resetLayout.call( this );
  // reset packer
  var parentSize = getSize( this.element.parentNode );
  var colW = this.columnWidth + this.gutter;
  this.fitWidth = Math.floor( ( parentSize.innerWidth + this.gutter ) / colW ) * colW;
  this.packer.width = this.fitWidth;
  this.packer.height = Number.POSITIVE_INFINITY;
  this.packer.reset();
};


Packery.prototype._getContainerSize = function() {
  // remove empty space from fit width
  var emptyWidth = 0;
  for ( var i=0, len = this.packer.spaces.length; i < len; i++ ) {
    var space = this.packer.spaces[i];
    if ( space.y === 0 && space.height === Number.POSITIVE_INFINITY ) {
      emptyWidth += space.width;
    }
  }

  return {
    width: this.fitWidth - this.gutter - emptyWidth,
    height: this.maxY - this.gutter
  };
};

// always resize
Packery.prototype.resize = function() {
  this.layout();
};


document.addEventListener("DOMContentLoaded", function(){



  jQuery('.wf-form').on('submit', function(e) {
      e.preventDefault();
      jQuery(that).find('.error').removeClass('show')
      jQuery(this).find('.loader-container').addClass('show');
      var data = jQuery(this).serialize();
      var that = this;
      jQuery.post(this.action, data, function(res){
       
        if (res.success) {
          var html = jQuery(res.data.html);
          jQuery('.wf-response').html(html);
          jQuery(that).find('.loader-container').removeClass('show');
          jQuery(that).addClass('hide');
          jQuery('.wf-response').addClass('show');
          jQuery(that).get(0).reset();
        } else {
          jQuery(that).find('.error').addClass('show')
        }
      })
   });


});

jQuery(window).load(function() {
	if (jQuery('.project-gallery').length) {
  jQuery('.project-gallery').addClass('metaslider metaslider-flex ml-slider').flexslider({
    animation: "slide",
    slideshowSpeed:3000,
    controlNav:true,
	directionNav:true,
	pauseOnHover:true,
	direction:"horizontal",
	reverse:false,
	animationSpeed:600,
	prevText:"&lt;",
	nextText:"&gt;",
	easing:"linear",
	slideshow:true
  });
	}

	app.createPackeryInstance = function() {
		var container = document.querySelectorAll('.packery');

		app.pckry = [].map.call(container, function(item) {
      return new Packery( item, {
		  itemSelector: '.grid-item',
		  columnWidth: 204,
		  gutter: 20,
		  transitionDuration: 0,
      isResizeBound: false
		});
    });
    console.log('packery instanstiated');
	};

  var w = jQuery(window).width();

  var isWide = !!(w >= 1100)

  if (isWide) {
  	app.createPackeryInstance();  
  }



});

(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize 
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');


// usage:
jQuery(window).smartresize(function(e){
	var w = jQuery(window).width();

	var isWide = !!(w >= 1100); // Create boolean

	console.log(isWide);

	if (isWide) {
		if (!app.pckry) {
			app.createPackeryInstance();
			//app.pckry.layout();
		}
	} else {
		if (app.pckry) {
			app.pckry.destroy();

      app.pckry = false;
      console.log('packery destroyed');
		}
	}

}, 500);




