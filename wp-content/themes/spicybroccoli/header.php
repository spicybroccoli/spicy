<?php

/**

 * The Header for our theme.

 *

 * Displays all of the <head> section and everything up till <div id="main">

 * 

 * @package Spicy Broccoli

 * @since Spicy Broccoli 1.0

 * @version 1.0

 */

?>
<!DOCTYPE html>

<html <?php language_attributes(); ?> <?php body_class(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style-responsive.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:400,900,300' rel='stylesheet' type='text/css'>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4fab20d5567ffc98" async></script>
<?php wp_head(); ?>
</head>

<body>
<div id="mp-pusher" class="mp-pusher">
<nav class="mp-menu" id="mp-menu">
  <div class="mp-level">
    <?php

	           	wp_nav_menu( 

	            	array( 

	            		'theme_location' => 'menu-pusher',

	            		'container'  => 'false',

	            		'container_id' => 'mp-menu',

	            		'menu_class' => 'mp-level',

	            		'container_class' => 'mp-menu',

	            		'menu_class' => '',  

	            		'walker' => new SBM_Responsive_Nav()

	            	) 

	            ); 

	        ?>
  </div>
</nav>
</div>
<!-- #mp-pusher -->
<div id="header">
  <div class="wrapper"> <a id="logo" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_sbm.png"  alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"/>
    </a>
    <?php wp_nav_menu( array( 'theme_location' => 'top-nav' ) ); ?>
    <span id="phone">02 8084 5554</span>
    <div id="access" role="navigation">
      <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
    </div>
    <!-- #access --> 
    <a href="#" id="trigger" class="menu-trigger"></a>
  </div>
  <!-- .wrapper--> 
  
</div>
<!-- #header -->
<div id="main">

<!--<div class="wrapper">-->

<?php the_breadcrumbs(); ?>

<!--</div>-->