<?php
/**
 * The default Page template
 * 
 * @package Spicy Broccoli
 * @since Spicy Broccoli 1.0
 * @version 1.0
 */

get_header(); ?>
	<div class="bg-white">
		<div class="wrapper cf">
		<?php get_template_part('partials/strip-quote'); ?>

								
		</div><!-- .wrapper -->
	</div><!-- .bg-white-->

	<?php get_template_part('partials/strip-case-studies'); ?>

<?php get_footer(); ?>