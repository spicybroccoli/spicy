<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_spicy');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=N+Uw;SU}5[`@WgWa/v=-Hyrk(x2U}GkR+gYRQ?{q4#/l?}a&SOt8;W8h:rmq-y?');
define('SECURE_AUTH_KEY',  'AS-+-6,y!/l$0b~aR96@+O}++h.Y$0 @1Vrn(9ufk]AGppW(o:U]|eY$+Y2S+J0T');
define('LOGGED_IN_KEY',    '50VA!%d-Op-F5+nqtvYm39uGDr8s(_rc1P2u(GSaiV;%zx_x&6PVZ9.b[@#ycw3S');
define('NONCE_KEY',        'juG1;Ex?JI-Teni{8v_3&mkYsQ<S-tg{K!.L2P/tNaT%.+8,z|ns2zliq}u-cYQ6');
define('AUTH_SALT',        'O]_R=pK/*(I87n%ZZVX5@&!sh}p3-WlhOF2iyQ!W?bEH=0)=A4*(L7dN:9irJNJ+');
define('SECURE_AUTH_SALT', 'Dz+5J*9Ku6v1Gc> 7=)CIfZ(Sbb4osad=Hk+?M>Px>>gd oD0O|Q{&,`L-Syu=4-');
define('LOGGED_IN_SALT',   '-k`FVOVS|K6%ce1x<.;<!s-6TQ;7V*z`o+H{xcg8[@#*ii:p{sqnP_:$-%sLzrz2');
define('NONCE_SALT',       'iv*2XdM lJo7bL51<7/<pj#,J|f=j$CS:wwW_dl]ipt.|SwAjSWP[Pe$$f(GFUcP');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
